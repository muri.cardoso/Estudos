class User{
    constructor(name, gender, birth, country, email, password, photo, admin){
        this._id;
        this._name = name;
        this._gender = gender;
        this._birth = birth;
        this._country = country;
        this._email = email;
        this._password = password;
        this._photo = photo;
        this._admin = admin;
        this._register = new Date();
    }
    get id() {
        return this._id;
    }
    get register (){
        return this._register;
    }
    get name() {
        return this._name;
    }
    /*set name() {
        return this._name;
    }//Close name*/
    get gender() {
        return this._gender;
    }//close gender
    get birth() {
        return this._birth;
    }//close birth
    get country() {
        return this._country;
    }//close country
    get email() {
        return this._email;
    }//close email
    get password() {
        return this._password;
    }//close password
    get photo() {
        return this._photo;
    }//close photo
    get admin() {
        return this._admin;
    }//close admin

    set photo(value){
        this._photo = value
    }
    loadFromJSON(json) {
        for(let name in json){
            switch (name) {
                case '_register':
                    this[name] = new Date(json[name]);
                break;
            
                default:
                    if(name.substring(0,1)==="_")this[name] = json[name];
                    break;
            }
            
        }
    }
    static getUsersStorage() { // retorna valores do local Storage
        return HttpRequest.get(`/users/`)
    }
    getNewID() {
        let usersID = parseInt(localStorage.getItem('usersID'));
        if(!usersID > 0) usersID = 0;
        usersID++;
        localStorage.setItem('usersID', usersID)
        return usersID;
    }
    toJSON() {
        let json = {}
        Object.keys(this).forEach(key =>{
            if(this[key] !== undefined) json[key] = this[key]
        })
        return json;
    }
    save() {
        return new Promise((resolve, reject)=>{
            let promise;
            console.log(resolve)
            if (this.id) {
                promise = HttpRequest.put(`/users/${this.id}`, this.toJSON());
            }else {
                console.log(this.toJSON())
                promise = HttpRequest.post(`/users`,this.toJSON());
            }
            promise.then(data =>{
                console.log(resolve);
                this.loadFromJSON(data);
                resolve(this)
            }).catch(e=>{
                reject(e)
            });
        })
       
    }
    remove() {
      return HttpRequest.delete(`/users/${this.id}`)
    }
}