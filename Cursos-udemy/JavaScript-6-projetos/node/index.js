const express = require('express');//express module
const consign = require('consign'); //consign module
const bodyParser = require('body-parser');//body-parser module utilizado para o metodo post
const expressValidator = require('express-validator');
let app = express();//var to util express
app.use(bodyParser.urlencoded({extended:false}));//use bodyParser with urlencode
app.use(bodyParser.json());//archive util to bodyParser
app.use(expressValidator())
consign().include('routes').include('utils').into(app);
//consign module a ser instalado
app.listen(3000, '127.0.0.1',()=>{
    console.log('server is runnig')
})