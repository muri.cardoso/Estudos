let NeDB = require('nedb');
let db = new NeDB({
    filename: 'users.db',
    autoload:true
})
module.exports = app=>{
    let route = app.route('/users')
    route.get((req, res)=>{
        db.find({}).sort({name:1}).exec((err, users)=>{
            if(err){
               app.utils.error.send(err,req,res);
            }else {
                res.status(200).json(users);
            };
        });
    });
    /*------------------------------------metodo post para seta um novo usuario postar os dados---------------------------------------- */
    route.post((req, res)=>{//para usar o metodo post com post man, necessário intalar um module para isso
       if(! app.utils.validator.user(app,  req, res)) return false
        db.insert(req.body, (err, user)=>{
            if(err) {
                app.utils.error.send(err,req,res);
            }else {
                res.status(200).json(user);
            }
        });
    });
    /*---------------------------------metodo get para procurar 1 usuario-------------------------------------------------------------* */
    let routeId = app.route('/users/:id');
    routeId.get((req, res) => {
        db.findOne({_id:req.params.id}).exec((err, user) => {//procurando usuário por id 
            if(err) {
                app.utils.error.send(err,req,res);
            }else {
                res.status(200).json(user);
            }
        });
    });
    /*---------------------------------Método PUT-----------------------------------------------*/
    routeId.put((req, res) => { //trocar informações do usuário
        if(! app.utils.validator.user(app,  req, res)) return false
        db.update({_id:req.params.id},req.body, err => {//procurando usuário por id 
            if(err) {
                app.utils.error.send(err,req,res);
            }else {
                res.status(200).json(Object.assign(req.params, req.body));
            }
        });
    });
    /*-----------------------------------------------metodo Delete, exclusão com RestAPI--------------------------------------------------*/ 
    routeId.delete((req, res)=>{
        db.remove({_id:req.params.id},{}, err=>{
            if(err) {
                app.utils.error.send(err,req,res);
            }else {
                res.status(200).json(req.params.id);
            }
        });
    })
};