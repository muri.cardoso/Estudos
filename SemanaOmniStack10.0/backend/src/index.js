const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes.js');

const app = express();

mongoose.connect('mongodb+srv://muricardoso:dev123456@cluster0-dnk90.mongodb.net/week10?retryWrites=true&w=majority',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(express.json());
app.use(routes)
//métodos HTPP get (pegar), post(cadastro), put(editar informações), delete(deletar infos) métodos de utilização

// Tipo de parâmetros:
//Query Params: (geralmente no get) request.query(filtros, ordenação, paginação, ...)
//Route Params:(geralmente put e delete) request.params(Identificar um recurso na alteração ou remoção)
// app.delete('/users/:id',(request, response)=>{
//     //return response.send('envio de mensagem escrita no html');
//     console.log(request.params)
//     return response.json({message:"hello world"}); Post
// })
//Body: request.body(Dados para criação ou alteração de um registro)

//MongoDB(Não-relacional)



app.listen(3333);  