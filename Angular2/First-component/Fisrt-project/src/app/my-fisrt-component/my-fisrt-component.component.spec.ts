import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFisrtComponentComponent } from './my-fisrt-component.component';

describe('MyFisrtComponentComponent', () => {
  let component: MyFisrtComponentComponent;
  let fixture: ComponentFixture<MyFisrtComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFisrtComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFisrtComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
