var idUf;
function populateUFs() {
    const ufselect = document.querySelector('select[name=uf]');
    const url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados"
    fetch(url).then(res => res.json()).then( data => {
        for (dataItem of data) {
            ufselect.innerHTML += `<option  value="${dataItem.id}">${dataItem.nome}</option>`;
        }
    })
}
populateUFs()
function getCities(event) {
    const citySelect = document.querySelector("select[name=city]");
    const stateInput = document.querySelector("input[name=state]");

    const ufValue = event.target.value;
    const indexOfSelectState = event.target.selectedIndex;
    stateInput.value = event.target.options[indexOfSelectState].text;

    const url = `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${ufValue}/municipios`;  
    citySelect.innerHTML = '<option value>Selecione a Cidade</option>.';
    citySelect.disabled = true; 
    fetch(url).then(res => res.json()).then( cities => {
        for ( city of cities) {
            citySelect.innerHTML += `<option  value="${city.nome}">${city.nome}</option>`;
        }
        citySelect.disabled = false;
    })
    
}//getCities finish
document.querySelector('select[name=uf]').addEventListener('change', getCities);

//itens de coleta
const itemsToCollect = document.querySelectorAll(".items-grid li");

for(const item of itemsToCollect) {
    item.addEventListener("click", handleSlectedItem);
}
const collectedItems = document.querySelector('input[name=items]');
let selectedItems = [];
function handleSlectedItem() {
    const itemLi = event.target;
    const itemId = event.target.dataset.id;//data html - algo.
    
    itemLi.classList.toggle('select');
    //verificar se tem itens selecionados
    const alreadySelected = selectedItems.findIndex(item => item == itemId)//true or false
    if (alreadySelected >= 0) {
        //tirar a seleção
        const filteredItems = selectedItems.filter(item =>{
             const itemIsDiferente = item != itemId;
             return itemIsDiferente;
        })
        selectedItems = filteredItems;
    }else {
        selectedItems.push(itemId);
    }
   collectedItems.value = selectedItems;
    
    //pegar valores
}