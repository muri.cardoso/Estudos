export default class OptionSelectCreate {
    static createOption (url, optionSelect) {
        fetch(url).then(res => res.json()).then( data => {
            for (dataItem of data) {
                optionSelect.innerHTML += `<option value="${dataItem.id}">${dataItem.nome}</option>`;
            }
        })
    }
}