const express = require("express")// requisição do express para ser utilizado
const server = express()// execução do express e atribuição em outra const
//pegar banco de dados
const db = require("./dataBase/db")
//configurar pasta publica
server.use(express.static("public"))
//hjabilitar o uso do req.body
server.use(express.urlencoded({extended:true}))

//utilizando template engine 
const nunjucks = require("nunjucks")
nunjucks.configure("src/views",{
    express: server,
    noCache: true
})
//conjfigurar rotas da aplicação
//pagina inicial
//req: pergunta
//res: resposta
server.get("/",(req, res) => {
    return res.render("index.html")
})
server.get("/create-point",(req, res) => {
    //pegar os dados do db
    //para pgar dados dos inputs, req.query query strings
    
    return res.render("create-point.html")
})
server.post("/savepoint",(req, res)=> {
    //req.body para retorno dos dados, pelo method post
    // console.log(req.body)

    //inserir dados do banco de dados
    const query = `
        INSERT INTO places (
            image,
            name,
            address,
            address2,
            state,
            city,
            items
        ) VALUES (?,?,?,?,?,?,?);
    `
    const values = [
        req.body.image,
        req.body.name,
        req.body.address,
        req.body.address2,
        req.body.state,
        req.body.city,
        req.body.items
    ]
    function afterInsertData(err) {
        if(err) {
             console.log(err);
             return res.send("erro de cadastro")
        };
        console.log("Cadastrado com sucesso");
        console.log(this);
        return res.render("create-point.html", {saved: true})
    }
    db.run(query, values, afterInsertData)      
    
})
server.get("/search",(req, res) => {
    const search = req.query.search
    if(search == "") {
        //pesquisa vazia
        return res.render("search-results.html", {total: 0 })
    }
    db.all(`SELECT * FROM places WHERE city LIKE '%${search}%'`, function(err, rows) {
        if(err) {
            return console.log(err);
        };
        const total = rows.length
        console.log("aqui estao seus registros");
        console.log(rows)
        return res.render("search-results.html", { places: rows, total })
    })
    
})
//ligar o servidor
server.listen(3000)//ouvir porta 3000

