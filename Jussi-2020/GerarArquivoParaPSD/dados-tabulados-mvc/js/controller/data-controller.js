class dataController{
    constructor(){
        this._inputsKey =document.querySelectorAll('input[data-key]');
        this._inputsVal =document.querySelectorAll('input[data-val]');   
        this.keys = this.mapInputsData(this._inputsKey);
        this.val = this.mapInputsData(this._inputsVal);
        this._dataTable = {};
        this.generateData();
    }
    mapInputsData(obj){
        let inputs = obj;     
        let teste =  [].map.call(inputs, function(obj) {
            return obj.value;
        });
      return teste
    }
    generateData(){
        for (var i = 0; i < this.keys.length; i++) {
            let key = this.keys[i];
            let val = this.val[i];
            this._dataTable[key] = val;
        }
    }
    get inputsKey(){
        return this._inputsKey;
    }
    set inputsKey(val){
        this.keys = this._inputsKey;
    }
}