var fs = require('fs');

var read_stream = fs.createReadStream('index.html', {encoding: 'utf8'});
read_stream.on("data", function(data){
    
    // var ent = require('ent');
        // console.log(ent.encode(data))
        // console.log(ent.decode());
        const Entities = require('html-entities').Html5Entities;
  
        const entities = new Entities();
        console.log(entities.encode(data)); // &lt;&gt;&quot;&amp;&copy;&reg;∆
        
});
read_stream.on("error", function(err){
    console.error("An error occurred: %s", err)
});

/*recolhe url*/
