# Email Preparer
Resets an HTML template for hosting.

### Requisitos
[NodeJs v10+](https://nodejs.org/en/) + NPM

#### Instala as dependência do projeto
```install
npm i
```

### Prepara os arquivos com base no arquivo `config.json`
```run
node .\index.js 'C:\path\of\my\project'
```

### Arquivo de configuração `config.json`
```configuration
{
	"key": "uniqueProjectKey",
	"fileIn": "index.html",
	"fileOut": "index-local.html",
	"entities": true,
	"zip": true,
	"assetsIn": "./images/",
	"assetsOut": "https://www.myNewUrl.com/"
}
```
O arquivo “config.json” é responsável em direcionar o “Email Preparer” na tratativa dos arquivos “.html”.

### Estrutura de arquivos
```structure
├── example
│	├── build
│	│	├── images
│	│	├── accessibility-report.html
│	│	├── index.html
│	│	└── index-local.html
│	├── images
│	├── config.json
│	└── index.html
```

### Endereço local
[http://localhost:9991](http://localhost:9991)
OBS: O servidor é iniciado para o serviço [lighthouse](https://developers.google.com/web/tools/lighthouse/) ler o arquivo e gerar o relatório. É encerrado assim que o processo termina!