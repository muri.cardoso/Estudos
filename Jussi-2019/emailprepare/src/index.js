'use strict';

const fs = require('fs-extra');
const path = require('path');
const async = require('async');
const prompt = require('prompt');
const archiver = require('archiver');
const cheerio = require('cheerio');
const lighthouse = require('lighthouse');
const lighthousePrinter = require('lighthouse/lighthouse-cli/printer');
const chromeLauncher = require('chrome-launcher');
const express = require('express');

// const args = process.argv;
// console.log(args);

const source = process.argv[2];
const dirName = path.basename(source).split(path.sep).pop();
const srcPath = path.resolve(__dirname)+'\\';
const tgtPath = path.resolve(source)+'\\';
const outPath = tgtPath + 'build\\';

const server = express();
const router = express.Router();
const port = 9991;

let data = fs.readFileSync(tgtPath + 'config.json');
let config = JSON.parse(data);

const {
	fileIn,
	fileOut,
	entities,
	zip,
	assetsIn,
	assetsOut
} = config;

let srcfileIn = `${tgtPath}${config.fileIn}`
let bldFileIn = `${outPath}${config.fileIn}`
let bldFileOut = `${outPath}${config.fileOut}`



const readFiles = async(file) => {
	return new Promise(resolve => {
		fs.readFile(file, 'utf8', (err, data) => {
			if (err) throw err
			console.log('The file has been readed!')

			resolve(data);
		})
	})
}

const writeFiles = async(file, content) => {
	fs.writeFileSync(file, content, (err) => {
		if (err) throw err
		console.log('The file has been saved!')
	})
}

const copyFiles = async(newPath, outPath) => {
	let from = `${newPath}images\\`
	let to = `${outPath}images\\`

	fs.existsSync(outPath) || fs.mkdirSync(outPath);
	fs.copy(to, from);
}

const prepareFiles = async(inFile, outFile, outPath) => {
	const $ = cheerio.load(await readFiles(inFile), {
		xml: {
			withDomLvl1: true,
			normalizeWhitespace: false,
			xmlMode: false,
			decodeEntities: entities
		}
	})
	let newContent = $.html()
	let $mainTable = $('body > table')
	let $images = $('img')

	if($mainTable.filter('[role="main"]')) {
		$('body > table').attr('role', 'main')
	}

	if(outFile == bldFileIn){
		$images.each(function () {
			let newSrc = $(this).attr('src').replace(/(images\/)/gim, config.assetsOut)
			$(this).attr('src', newSrc)
		});
	}

	newContent = $.html()

	await writeFiles(outFile, newContent)
}

const initServer = async(inPath, inFile) => {
	router.get('/', (req, res) => {
		res.sendFile(path.join(inFile))
	})
	server.use(express.static(inPath))
	server.use('/', router)
	server.listen(process.env.port || 9991)
	console.log(`Server is running at Port ${port}`)
}

const checkAccessibility = async(inFile) => {
	const opts = {
		output: 'html',
		outputPath: outPath + 'accessibility-report.html',
		chromeFlags: ['--headless'],
		onlyCategories: ['accessibility']
		// emulatedFormFactor: 'none'
	}
	const url = `http://localhost:${port}/${inFile}`

	await launchChromeAndRunLighthouse(url, opts)
		.then(results => {
			lighthousePrinter
				.write(results.report, opts.output, opts.outputPath)
				.then(() => {
					// open(opts.outputPath, { wait: false });
					// process.exit()
				})
		})
}

const launchChromeAndRunLighthouse = async(url, opts, config = null) => {
	return chromeLauncher.launch({chromeFlags: opts.chromeFlags}).then(chrome => {
		opts.port = chrome.port;
		console.log('Testing accessibility in HTML file');

		return lighthouse(url, opts, config).then(results => {
			console.log('Test completed');

			return chrome.kill().then(() => results)
		});
	})
}

const zipFiles = async(inPath, outPath, fileName) => {
	const archive = await archiver('zip', {
		zlib: {level: 9}
	});
	const srcFile = `${outPath}${fileName}.zip`;

	archive
		.directory(inPath, false, {date: new Date()})
		.on('error', (err) => reject(err))
		.pipe(fs.createWriteStream(srcFile))
		.on('close', () => {
			console.log(archive.pointer() + ' total bytes')
			console.log('archiver has been finalized and the output file descriptor has closed.')
		})
	;
	archive.finalize()
}
const end = async() => {
	return process.exit()
}
const build = async() => {
	await copyFiles(outPath, tgtPath)
	await prepareFiles(srcfileIn, bldFileIn, outPath)

	await prepareFiles(srcfileIn, bldFileOut, outPath)
	await initServer(outPath, bldFileIn)
	await checkAccessibility(fileIn)
	zip && await zipFiles(outPath, tgtPath, dirName)

	// await end()
}
build()