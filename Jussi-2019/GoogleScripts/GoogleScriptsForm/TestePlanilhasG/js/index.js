
var idFadeModalLimpaCampo = document.getElementById('modal-confirm').attributes.id.value,
	idFadeModalDadosEnviados = document.getElementById('modal-dados-enviados').attributes.id.value,
	idFadeModalPreenchaCampos = document.getElementById('modal-preencher-campos').attributes.id.value,
	artId = document.getElementById("artID"),
	artName = document.getElementById("artName"),
	artType = document.getElementById("artType"),
	artYear = document.getElementById("artYear"),
	artPrice = document.getElementById("artPrice"),
	artOld = document.getElementById("artOld"),
	artText = document.getElementById("artText"),
	artCar = document.getElementById("artCar");


function fadeOutInModal(idFade){let ModalFade = idFade; $(`#${ModalFade}`).fadeToggle(1,()=>{}); removeAddBlur()}
function removeAddBlur(){let element = document.getElementById("blur");element.classList.toggle("blur");/* $('#checklist').toggleClass('modal')*/}
function removeValueInput(){$('.input-dados').val("")}

function openModal(){
	let idFade = idFadeModalLimpaCampo
	fadeOutInModal(idFade)	
}


$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
				
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		
		}
	});
	
	return o;
};  

function enviarDados(){
	let vazioId = artId,
		vazioName = artName,
		vazioType = artType,
		vazioYear = artYear,
		vazioPrice = artPrice,
		vazioOld = artOld,
		vazioText = artText,
		vazioCar = artCar;

	preencherCampos = setInterval(function(){
		vazioId.value == "" ? vazioId.classList.add('preencher') : vazioId.classList.remove('preencher')
		vazioName.value == "" ? vazioName.classList.add('preencher') : vazioName.classList.remove('preencher')
		vazioType.value == "" ? vazioType.classList.add('preencher') : vazioType.classList.remove('preencher')
		vazioYear.value == "" ? vazioYear.classList.add('preencher') : vazioYear.classList.remove('preencher')
		vazioPrice.value == "" ? vazioPrice.classList.add('preencher') : vazioPrice.classList.remove('preencher')
		vazioOld.value == "" ? vazioOld.classList.add('preencher') : vazioOld.classList.remove('preencher')
		vazioText.value == "" ? vazioText.classList.add('preencher') : vazioText.classList.remove('preencher')
		vazioCar.value == "" ? vazioCar.classList.add('preencher') : vazioCar.classList.remove('preencher')
	},100)


	if(vazioId.value == "" || vazioName.value == "" || vazioType.value == "" || vazioYear.value == "" || vazioPrice.value == "" || vazioOld.value == "" || vazioText.value == "" || vazioCar.value == "" ){
		let idFade = idFadeModalPreenchaCampos
		fadeOutInModal(idFade)
		
		

		var refreshIntervalId =	setInterval(function(){
		fadeOutInModal(idFade) 
		return stop
	   }, 1500);
	   setInterval(function(){
		clearInterval(refreshIntervalId);
	   }, 2000);

	}else{
		var $form = $('form#test-form'),
			url = 'https://script.google.com/macros/s/AKfycbzM0c23pHPaeZEhJfc54mgG1cqyxrrqFYQMmZobk1OWjxpFKV9u/exec',
			numberArt = document.getElementById('artboard').value
			
		
		$('#submit-form').on('click', function(e) {
		e.preventDefault();
		var jqxhr = $.ajax({
			url: url,
			method: "GET",
			dataType: "json",
			data: $form.serializeObject(),
			dataNumber: numberArt
		})

		console.log(numberArt)
		})
		let idFade = idFadeModalDadosEnviados
		fadeOutInModal(idFade)
		var refreshIntervalId =	setInterval(function(){
		fadeOutInModal(idFade) 
		return stop
	   }, 2000);
	   setInterval(function(){
		clearInterval(refreshIntervalId);
	   }, 2500);	  
	
		
	
	}
	
}

function exitModal(){
	let idFade = idFadeModalLimpaCampo
	fadeOutInModal(idFade)
}

function confirmClear(){
	let idFade = idFadeModalLimpaCampo
	
	removeValueInput()	
    fadeOutInModal(idFade)
    clearInterval(preencherCampos);
	   
}


